<?php

namespace clases;

class Superheroe
{
    private string $nombre;
    private string $descripcion;
    private bool $capa;

    public function __construct(string $nombre)
    {
        $this->nombre = $nombre;
        $this->descripcion = "";
        $this->capa = false;
    }



    /**
     * Get the value of nombre
     *
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @param string $nombre
     *
     * @return self
     */
    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of descripcion
     *
     * @return string
     */
    public function getDescripcion(): string
    {
        return $this->descripcion;
    }

    /**
     * Set the value of descripcion
     *
     * @param string $descripcion
     *
     * @return self
     */
    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get the value of capa
     *
     * @return bool
     */
    public function getCapa(): bool
    {
        return $this->capa;
    }

    /**
     * Set the value of capa
     *
     * @param bool $capa
     *
     * @return self
     */
    public function setCapa(bool $capa): self
    {
        $this->capa = $capa;

        return $this;
    }

    public function __toString(): string
    {
        $salida = "<h2>Datos del superhéroe</h2>";
        $salida .= "<p>Nombre: " . $this->nombre . "</p>";
        $salida .= "<p>Descripción: " . $this->descripcion . "</p>";
        $salida .= "<p>Capa: " . ($this->capa ? "Si" : "No") . "</p>";
        return $salida;
    }
}
