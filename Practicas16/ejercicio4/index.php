<?php

use clases\Superheroe;

spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

$heroe1 = new Superheroe("batman");
$heroe1->setCapa(true);

//CUANDO IMPRIMO EL OBJETO ENTONCES
//SE IMPRIMIRAN LOS ATRIBUTOS DE LA CLASE

echo $heroe1;
