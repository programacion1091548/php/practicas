<?php

namespace clases\ejercicio2;

class Persona
{
    public  string $nombre;
    public  string $apellidos;
    public  string $numeroDocumentoIdentidad;
    public int $añoNacimiento;
    public string $paisNacimiento;
    public string $sexo;

    public function __construct( 
        string $nombre="", 
        string $apellidos="", 
        string $numeroDocumentoIdentidad="", 
        int $añoNacimiento=0,
        string $paisNacimiento="",
        string $sexo=""
        )
    {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->numeroDocumentoIdentidad = $numeroDocumentoIdentidad;
        $this->añoNacimiento = $añoNacimiento;
        $this->paisNacimiento = $paisNacimiento;
        $this->sexo = $sexo;
    }

public function imprimir() {
    // Utilizar un div para encapsular la información de la persona
    return "<div class='cajaPersona'>
                <h2>{$this->nombre} {$this->apellidos}</h2>
                <p>DNI: {$this->numeroDocumentoIdentidad}</p>
                <p>Año de nacimiento: {$this->añoNacimiento}</p>
                <p>Pais de nacimiento: {$this->paisNacimiento}</p>
                <p>Sexo: {$this->sexo}</p>
            </div>";
}
}