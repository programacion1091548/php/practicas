<?php

use clases\ejercicio1\Persona;

require_once "clases/Persona.php";

// $persona1 = new Persona("Juan", "Peña", "12345678A", 1990);

// $persona2 = new Persona("Marián", "Peñas", "87654321B", 1990);

// echo $persona1->imprimir();
// echo $persona2->imprimir();



$persona1 = new Persona("Juan", "Peña", "12345678A", 1990);
$persona2 = new Persona("Marián", "Peñas", "87654321B", 1990);

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Información de Personas</title>
    <style>
        /* Estilo para las cajas de persona */
        .cajaPersona {
            width: 45%;
            display: inline-block;
            vertical-align: top;
            box-sizing: border-box;
            padding: 10px;
            margin: 5px;
            border: 1px solid #ccc;
        }
    </style>
</head>
<body>

<!-- Imprimir la información de las personas en las cajas -->
<?php echo $persona1->imprimir(); ?>
<?php echo $persona2->imprimir(); ?>

</body>
</html>





