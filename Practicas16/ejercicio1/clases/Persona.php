<?php

namespace clases\ejercicio1;

class Persona
{
    public  string $nombre;
    public  string $apellidos;
    public  string $numeroDocumentoIdentidad;
    public int $añoNacimiento;

    public function __construct( 
        string $nombre="", 
        string $apellidos="", 
        string $numeroDocumentoIdentidad="", 
        int $añoNacimiento=0,
        )
    {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->numeroDocumentoIdentidad = $numeroDocumentoIdentidad;
        $this->añoNacimiento = $añoNacimiento;
    }

public function imprimir() {
    // Utilizar un div para encapsular la información de la persona
    return "<div class='cajaPersona'>
                <h2>{$this->nombre} {$this->apellidos}</h2>
                <p>DNI: {$this->numeroDocumentoIdentidad}</p>
                <p>Año de nacimiento: {$this->añoNacimiento}</p>
            </div>";
}
}