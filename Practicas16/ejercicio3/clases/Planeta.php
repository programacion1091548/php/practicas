<?php
namespace clases;

class Planeta {
    public  string $nombre;
    public int $cantidadSatelites;
    public float $masa;
    public  float$volumen;
    public  int $diametro;
    public int $distanciaAlSol;
    public  string $tipo;
    public bool $observable;

    // Constructor
    public function __construct(string $nombre=null, int $cantidadSatelites=0, float $masa=0, float $volumen=0, int $diametro=0, int $distanciaAlSol=0, string $tipo="", bool $observable=false) {
        $this->nombre = $nombre;
        $this->cantidadSatelites = $cantidadSatelites;
        $this->masa = $masa;
        $this->volumen = $volumen;
        $this->diametro = $diametro;
        $this->distanciaAlSol = $distanciaAlSol;
        $this->tipo = $tipo;
        $this->observable = $observable;

        // public function __construct(string $nombre, int $cantidadSatelites, float $masa, float $volumen, int $diametro, int $distanciaAlSol, string $tipo, bool $observable) {
        //     $this->nombre = null;
        //     $this->cantidadSatelites = 0;
        //     $this->masa = 0;
        //     $this->volumen = 0;
        //     $this->diametro = 0;
        //     $this->distanciaAlSol = 0;
        //     $this->tipo = "";
        //     $this->observable = false;
        }
    

    // Método para imprimir los valores de los atributos
    public function imprimirDatos() 
    {
        echo "Nombre: {$this->nombre}\n";
        echo "<br>";
        echo "Cantidad de Satélites: {$this->cantidadSatelites}\n";
        echo "<br>";
        echo "Masa: {$this->masa} kg\n";
        echo "<br>";
        echo "Volumen: {$this->volumen} km³\n";
        echo "<br>";
        echo "Diámetro: {$this->diametro} km\n";
        echo "<br>";
        echo "Distancia al Sol: {$this->distanciaAlSol} millones de km\n";
        echo "<br>";
        echo "Tipo: {$this->tipo}\n";
        echo "<br>";
        echo "Observable a simple vista: " . ($this->observable ? "Sí" : "No") . "\n";
        echo "<br>";
       
    }
public function imprimir(): string 

{
    $salida="<h2>".$this->nombre."</h2>";
    $salida.="<p>Cantidad de Satelites: ".$this->cantidadSatelites."</p>";
    $salida.="<p>Masa: ".$this->masa."</p>";
    $salida.="<p>Volumen: ".$this->volumen."</p>";
    $salida.="<p>Diámetro: ".$this->diametro."</p>";
    $salida.="<p>Distancia al Sol: ".$this->distanciaAlSol."</p>";
    $salida.="<p>Tipo: ".$this->tipo."</p>";
    $salida.="<p>Observable a simple vista: " . ($this->observable ? "Sí" : "No") . "</p>";
    
    return $salida;
}
//añadir un getter para observable
private function getObservable():string
{   
    $salida=$this-> observable ? "Sí" : "No";
    return $salida;
}

    // Método para calcular la densidad del planeta
    public function calcularDensidad() {
        return $this->masa / $this->volumen;
    }

    // Método para determinar si un planeta es exterior al sistema solar
    public function esExterior() {
        $distanciaEnUA = $this->distanciaAlSol / 149597870; 
        return $distanciaEnUA > 3.4 ;
    }
}