<?php
use clases\Planeta;


require_once "autoload.php";
$planeta1= new Planeta("Mercurio", 0, 3.3, 0.88, 4879, 57910000, "Terrestre", true);
$planeta2= new Planeta("Venus", 0, 8.87, 0.95, 12104, 108200000, "Terrestre", true);
$planeta3= new Planeta("neptuno", 14, 1025, 1407, 49244, 4495100000, "Gaseoso", false);
$planeta4= new Planeta("jupiter", 67, 1898, 1433, 1326, 778500000, "Gaseoso", false);
$planeta5= new Planeta ("saturno", 62, 5683, 1160, 1160, 1433500000, "Gaseoso", false);
$planeta6= new Planeta("urano", 27, 86832, 50724, 1271, 2870990000, "Gaseoso", false);
$planeta7= new Planeta("Madagascar", 45, 1000000, 2000000, 2000000, 0, "Gaseoso", false);



// muestro los atributos en pantalla

echo "Planeta 1:\n";
echo "<br>";
$planeta1->imprimirDatos();
echo "Densidad: {$planeta1->calcularDensidad()} kg/km³\n";
echo "<br>";
echo "Es un planeta exterior: " . ($planeta1->esExterior() ? "Sí" : "No") . "\n\n";
echo "<br>";
echo "<br>";


echo "Planeta 2:\n";
echo "<br>";
$planeta2->imprimirDatos();
echo "Densidad: {$planeta2->calcularDensidad()} kg/km³\n";
echo "<br>";
echo "Es un planeta exterior: " . ($planeta2->esExterior() ? "Sí" : "No") . "\n";

echo "<br>";
echo "<br>";
echo"Planeta 3:\n";
echo "<br>";
$planeta3-> imprimirDatos();
echo "Densidad: {$planeta3->calcularDensidad()} kg/km³\n";
echo "<br>";
echo "Es un planeta exterior: " . ($planeta3->esExterior() ? "Sí" : "No") . "\n";


