<?php

namespace clases\ejercicio2;

class Producto

{

    public int $idProducto;
    public string $nombre;
    public float $precio;
    public int $stock;
    public string $descripcion;

    public function __construct()
    {
        $this->idProducto = 0;
        $this->nombre = "";
        $this->precio = 0;
        $this->stock = 0;
        $this->descripcion = "";
    }



    /**
     * Get the value of idProducto
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Set the value of idProducto
     *
     * @return  self
     */
    public function setIdProducto($idProducto)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get the value of nombre
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of precio
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set the value of precio
     *
     * @return  self
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get the value of stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set the value of stock
     *
     * @return  self
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get the value of descripcion
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set the value of descripcion
     *
     * @return  self
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }


    public function mostrarDetalles(): string
    {
        $salida = "<h2>Datos del Producto</h2>";
        $salida .= "<p>ID: " . $this->idProducto . "</p>";
        $salida .= "<p>Nombre: " . $this->nombre . "</p>";
        $salida .= "<p>Precio: " . $this->precio . "</p>";
        $salida .= "<p>Stock: " . $this->stock . "</p>";
        $salida .= "<p>Descripción: " . $this->descripcion . "</p>";

        return $salida;
    }

    public function actualizarStock($cantidad): void
    {
        $this->stock = $cantidad;
    }

    public function calcularPrecioDescuento($porcentajeDescuento): string
    {
        $descuento = $this->precio - ($this->precio * $porcentajeDescuento / 100);
        $salida = "<p>Precio con descuento: " . $descuento . "</p>";
        return $salida;
    }
}
