<?php

namespace clases\ejercicio2;

class Usuario

{

    public int $idUsuario;
    public string $nombre;
    public string $email;
    public int $edad;
    public bool $activo;

    public function __construct()
    {
        $this->idUsuario = 0;
        $this->nombre = "";
        $this->email = "";
        $this->edad = 0;
        $this->activo = false;
    }



    /**
     * Get the value of activo
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set the value of activo
     *
     * @return  self
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get the value of edad
     */
    public function getEdad()
    {
        return $this->edad;
    }

    /**
     * Set the value of edad
     *
     * @return  self
     */
    public function setEdad($edad)
    {
        $this->edad = $edad;

        return $this;
    }

    
    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of idUsuario
     */ 
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * Set the value of idUsuario
     *
     * @return  self
     */ 
    public function setIdUsuario($idUsuario)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

   

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function mostrarInformacion(): string
    {
        $salida = "<h2>Datos del usuario</h2>";
        $salida .= "<p>ID: " . $this->idUsuario . "</p>";
        $salida .= "<p>Nombre: " . $this->nombre . "</p>";
        $salida .= "<p>Email: " . $this->email . "</p>";
        $salida .= "<p>Edad: " . $this->edad . "</p>";
        $salida .= "<p>Activo: " . ($this->activo ? "Si" : "No") . "</p>";
        return $salida;
    }


    public function activarUsuario(): void
    {
        $this->activo = true;
    }

    public function cambiarEdad($edadNueva): void
    {
        $this->edad = $edadNueva;
    }
}
