<?php


class Cadena {
    private string $valor;
    private int $longitud;
    private int $vocales;
    
    public function __construct(string $valor) {
        $this->valor = $valor;
    }
    
    public function getValor(bool $minusculas=false): string {
        if($minusculas){
            return strtolower($this->valor);
        }else{
            return $this->valor;
        }
    }
    
    public function setValor(string $valor): void {
        $this->valor = $valor;
    }


    public function getLongitud(): int {
        $this->calcularLongitud();
        return $this->longitud;
    }
    
    private function calcularLongitud():void{
        $this->longitud=strlen($this->valor);
    }

    public function getVocales(): int {
        $this->numeroVocales();
        return $this->vocales;
    }
    
    private function numeroVocales(){
        $vocales=['a','e','i','o','u'];
        $numero=0;
        foreach ($vocales as $vocal) {
            $numero+=substr_count($this->getValor(true), $vocal);
        }
        $this->vocales=$numero;
    }

 
    public function repeticionVocal(string $vocal):int{
        return substr_count($this->getValor(), $vocal);
    }

    
    public function setVocales(int $vocales): void {
        $this->vocales = $vocales;
    }
    
    public function setLongitud(int $longitud): void {
        $this->longitud = $longitud;
    }
    
}
