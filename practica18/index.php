<?php

// autocarga de clases
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';
});

$texto=new Cadena("Hola");

var_dump($texto);

// echo $texto->valor; // no se puede porque es privada

echo $texto->getValor(); // Hola
$texto->setValor("Hola Mundo");
echo $texto->getValor(true); // hola mundo

// echo $texto->longitud;  // no se puede porque es private

echo $texto->getLongitud(); // 10

echo "<br>";
echo $texto->getVocales();

echo "<br>";
echo $texto->repeticionVocal("o");

