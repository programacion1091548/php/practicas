<?php
$c = 60; // Asignando un número directamente mayor de 10 a la variable c

do {
    echo "<div>" . $c . "</div>"; // Mostrando el valor de c dentro de etiquetas div
    $c = $c - 2; // Restando 2 a c
} while ($c > 10); // Continuar mientras c sea mayor que 10
?>
