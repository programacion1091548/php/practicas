<?php
    // Crear un array con 4 notas
    $notas = [7.5, 8.2, 6.9, 9.1];

    // Mostrar las notas con un bucle for
    echo "<h2>Notas con bucle for:</h2>";
    for ($i = 0; $i < count($notas); $i++) {
        echo "Nota ", $i + 1, ": ", $notas[$i], "<br>";
    }

    // Mostrar las notas con un bucle foreach
    echo "<h2>Notas con bucle foreach:</h2>";
    foreach ($notas as $indice => $nota) {
        echo "Nota ", $indice + 1, ": ", $nota, "<br>";
    }
    ?>