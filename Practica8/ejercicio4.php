<?php
    // Introduce el número en una variable
    $numero = 8;

    // Variable para almacenar la suma
    $sumaImpares = 0;

    // Bucle para sumar números impares
    for ($i = 1; $i <= $numero; $i += 2) {
        $sumaImpares += $i;
    }

    // Mostrar el resultado
    echo "<p>Suma de números impares hasta $numero: $sumaImpares</p>";
    ?>