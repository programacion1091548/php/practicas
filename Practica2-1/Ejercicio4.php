<?php
$precio=10;
const iva=0.10;
$precioIva=0;
$precioFinal=0;
$precioIva=$precio* iva;
$precioFinal= $precio+ $precioIva;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .container {
            display: flex;
            align-items: center;
        }
        .image {
            flex: 1;
            max-width: 200px;
        }
        .list {
            flex: 2;
        }
    </style>
</head>
<body>
<div class="container">
        <img class="image" src="unicorn.png" alt="Descripción de la imagen">
    
    <ul class="list">
    <li>
            <?= "Precio ",$precio?>
        </li>
        <li>
            <?= "IVA ",$precioIva *10,"%"?>
        </li>
        <li>
        <strong><?= "Precio Total ",$precioFinal?></strong>
        </li>
    </ul>
    </table>
</body>
</html>